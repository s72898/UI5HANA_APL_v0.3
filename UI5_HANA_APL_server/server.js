/* Test server */

// TODO add sql statements from frontend into nodejs-app

var express = require('express');
var mysql = require('mysql');

// init backend app
var app = express();

// database definition
var connection = mysql.createConnection({
  host: 'localhost',
  user: 's72898',
  password: 's72898',
  database: 'apl_ui5_testdb'
});

// connect to mysql testdb
connection.connect(function(err) {
  if(err) {
    console.error(err);
  };
  console.log('Connected...');
});

app.post('/getentries', function(request, response) {
  var mySqlQuery = request.query.mysqlquery;
  var resultSet = [];

  connection.query(mySqlQuery, function(err, rows, fields) {
    if(err) {
      console.log(err);
      resultSet = err;
    } else {
      for(var i in rows) {
        console.log(rows[i]);
        resultSet.push(rows[i]);
      }
    }

    response.writeHead(200, "OK", {'Content-Type': 'text/plain'});
    response.write(JSON.stringify(resultSet));
    response.end();
  });
});

app.use(express.static('../UI5_HANA_APL'));

app.listen(3000, function() {
  console.log('Webserver listening on port 3000...');
});
