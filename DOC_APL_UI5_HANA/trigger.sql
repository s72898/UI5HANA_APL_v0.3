create trigger blocking_insert_after_felling before insert on zze_72898kt
for each row

BEGIN
 
IF (SELECT bm_id FROM zze_72898kt as kt JOIN inserted as i ON kt.bm_id = i.bm_id WHERE kt.action = 'Faellen') THEN
	SIGNAL sqlstate '45001' set message_text = "No way ! You cannot do this !";
END IF;

END#

delimiter ;
