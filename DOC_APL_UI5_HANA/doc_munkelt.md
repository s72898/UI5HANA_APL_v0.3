# Entwicklung einer SAPUI5-Web-Applikation mit Anbindung an eine SAP-HANA-Express-Datenbank über einen ESH-REST-Service

# 0. Grundlegendes

## 0.1 System und relevante Tools

Die Entwicklung der App fand auf [Ubnutu](http://releases.ubuntu.com/17.04/) unter Zuhilfenahme der IDE Webstorm von [JetBrains](https://www.jetbrains.com/webstorm/) statt. 

**Hinweis: Es empfiehlt sich anfangs auf einer nennenswerten Entwicklungsumgebung, wie JetBrains Webstorm, zu verwenden. Ein Studenten-Account ist kostenfrei verfügbar.** 

Die [SAP Web IDE](https://www.sap.com/developer/topics/sap-webide.html) gehört zum Standardrepertoire der SAP Cloud Platform, worunter die heutige Entwicklung von SAP-HANA-Anwendungen und UI5-/Fiori-Apps stattfindet. Dort können UI5- oder Fiori-Anwendungen schnell erstellt werden. Dort lassen sich ebenso nach dem WYSIWYG-Prinzip schnell Views individuell generieren.

Eine virtuelles Ubuntu 16.04 - Gnome Flashback Metacity - über VMWare steht zur Verfügung. Die Ordnerstruktur des Projekts ```UI5_HANA_APL``` stammt aus dem Download eines SAPWebIDE-Projekts, worauf im weiteren Verlauf eingegangen wird.

Eine MySQL-Testdatenbank soll als Ersatz für eine HANA-Express-DB dienen, diese ebenso auf dem OS installiert und eingerichtet ist. 

Ebenfalls wichtige Resourcen für die Verwendung der SAP-Module für die UI5-Applikation findet man auf [OpenUI5.org](http://openui5.org/download.html), die API-Referenz findet man auf [OpenUI5 Hana Ondemand](https://openui5.hana.ondemand.com/).

Anmeldung auf Ubuntu:

- User: ui5user
- Passwort: ui5


## 0.2 Aufgabe und Sinn

Sinn des Tutorials soll es sein, das Data-Binding-Konzept unter SAPUI5 zu verstehen und die neuen Technologien der SAP-HANA-Welt kennen zu lernen, welche die klassischen Technologien um beispielsweise ABAP abzulösen. SAP nutzt die Vorteile von Web-Applikationen sowie die Ressourcenverfügbarkeit über verteilte Systeme um flexibler zu werden. UI5 ist ein JavaScript-Framework. JavaScript (JS) ist heutzutage im Web-Bereich überall präsent. UI5 arbeitet unter anderem mit dem MVC-Pattern, wobei Views aus XML-Code bestehen können, dieses Datenformat wollen wir hier nutzen.

Folgende Aufgabe soll mittels diesen Tutorials teilweise mit UI5 umgesetzte werden:

<p style="margin-left: 30px;">
<i><b>Baumschule</b><br>
Erstellen Sie in SAP eine Baumschulenverwaltung. Die Verwaltung besteht aus zwei persistenten
Tabellen: einer Tabelle „ZZE_xxxxxBM“ für die BäuMe und eine Tabelle „ZZE_xxxxxKTN“ für die
AKTioNen, die mit dem jeweiligen Baum durchgeführt werden, wie z. B. Pflanzen, Gießen, Düngen,
Verschneiden, Ausgraben, Verkaufen und Fällen. Die xxxxx stehen für Ihre Bibliotheksnummer bzw.
die Zahl aus Ihrer s-Nummer. Die beiden Tabellen sind 1:n miteinander verknüpft. Die Tabelle der
Bäume enthält die ID des Baumes, seine Bezeichnung und das Anlagedatum des Datensatzes. Die
Tabelle der Aktionen enthält als Primärschlüssel die ID des zugehörigen Baumes und das Datum, zu
dem die Aktion durchgeführt worden ist. Zudem enthält sie ein Textfeld, in dem die jeweilige Aktion
beschrieben wird. Die ID des Baumes in der Tabelle der Aktionen ist zudem Fremdschlüssel, der auf
den Baum bzw. die Baumtabelle verweist. Es ist keine Aktion ohne zugehörigen Baum möglich.
Erstellen Sie jeweils ein Dynpro und eine Transaktion zum Anlegen, Anzeigen und Ändern von
Bäumen und Aktionen, wobei sich eine Aktion immer auf einen Baum beziehen muss.
Zusatzaufgabe </p>

<p style="margin-left: 30px;">
1: Fügen Sie Restriktionen bzw. Plausibilitätsprüfungen ein: Z. B. kann ein Baum nicht
mehr gegossen werden, nachdem er gefällt worden ist.
Zusatzaufgabe 
</p>

<p style="margin-left: 30px;">
2: Erstellen Sie einen Report über Bäume und Aktionen.</i>
</p>

<p style="margin-left: 30px;">Quelle: Prof. Dr. Thorsten Munkelt</p>

## 0.3 Konfigurieren und Programmieren

In Kapitel 1 wird schrittweise demonstriert, wie man aus der SAPWebIDE ein Projekt exportiert, dieses anpasst und lokal lauffähig macht. Dies ist optional. In Kapitel 2 beginnt man nun mithilfe einer Projektvorlage zu implementieren.

# 1.0 Getting Started - Wie lege ich ein Projekt auf meinem lokalen System an?

## 1.1 Installation des Projektes

Nach der Registrierung und Anmeldung bei der [SAP Web IDE](https://www.sap.com/developer/topics/sap-webide.html) wählt man Trials/Downloads ==> SAP Cloud Platform Start your free trial. **Eventuell muss man sich an dieser Stelle erneut anmelden!**

Dann 'Europe (Rot) - Trial' ==> p-XXXX...trial auswählen. (Abb. [Europe Trial](#europe_trial))

[Europe Trial]: europe_trial.png "Europe Trial"
![Europe Trial][Europe Trial] 

Auf der linken Seite, der Sidebar, kann auf 'Services' nun die **SAP Web IDE** ausgewählt werden, mit 'Got to Service' gelangt man direkt dort hin.

Nun kann man unter Files ==> New ==> Project from Template (Abb. [Template](#template)) eine SAPUI5 Application erstellen. Der Projektname ist frei wählbar, der Namespace kann frei bleiben. In diesem Tutorial wählt man am besten als ViewType ```XML``` und als ViewName ```Home``` und drückt anschließend auf ```Finish```. *Für unser Tutorial empfiehlt sich als Projektname: ```UI5WebApp```*.

[template]: template.png "Template"
![Template][template]

Mit einem Rechtsklick auf das erstellte Projekte kann man dieses exportieren. (Abb. [Export](#export)) Es empfiehlt sich auf einer IDE außerhalb der SAPWebIDE zu arbeiten, da wir den Node.js-Server mit Middleware und MySQL-Anbindung installieren wollen. Anschließend soll die Zip noch entpackt werden. Optional kann die App testweise sofort mit ```Alt+F5``` gestartet werden. Das Projekt kann im *Downloads-Ordner* bleiben.

[export]: export.png "Export"
![Export][export]

Unter Webstorm wollen wir nun den Server einrichten und die Export-Zip integrieren. Zuerst legt man dabei einen neuen Oberordner an mit dem Namen 'UI5App', darin verschiebt man das extrahierte Projekt, UI5WebApp, und öffnet danach Webstorm in Ubuntu unter Application ==> Programming ==> Webstorm ==> Open wählt man den entsprechenden Ordner. (Abb. [Open](#open))

[open]: open.png "Open"
![Open][open]

Als nächstes werden die benötigten SAP-Resourcen eingebunden. Diese findet man auf [http://openui5.org/download.html](http://openui5.org/download.html). Dazu lädt man 'OpenUI5 SDK' herunter und entpackt dieses. Darunter findet man den Ordner **resources**. Dieser wird dann als Unterordner in das Projekt hineinkopiert, die Indizierung kann einige eine kurze Zeit in Anspruch nehmen.

Nach der Einbindung sollte die Projektstruktur wie folgt aussehen: (Abb. [Struktur](#struktur))

[struktur]: struktur.png "Struktur"
![Struktur][struktur]

Des weiteren soll ein weiterer Ordner für den Node.js-Server entstehen

## 1.2. Installation der NPM- und SAP-Module sowie des Node.js-Servers

Sinn: Es soll nachvollzogen werden, wie die Einbindung wichtiger Resourcen und die Anpassung von Dateien den Start der Entwicklung ermöglichen.

<!--(Die obigen Ordner sind bereitgestellt worden, weil keine node.js-Entwicklung vorgenommen werden sollte. TODO Wo kommen die Ordner her? Was ist in ihnen angepasst worden? Wozu dienen sie?)-->

### 1.2.1. Node.js-Server

<!--TODO obige Ordner anlegen lassen und Projekt in den einen verschieben (Dateisystem)-->

Der neue Ordner, worin sich unter Server befindet, heißt ```UI5WebServer```.

### 1.2.2. package.json 

Die package.json dient als Standard für den Node Package Manager (NPM) und ist im Datenformat eine JSON-File:

```json
{
  "name": "ui5hanaaplbackend",
  "version": "1.0.0",
  "main": "server.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1",
    "start": "node server.js"
  },
  "author": "",
  "license": "ISC",
  "dependencies": {
    "express": "^4.15.4",
    "mysql": "^2.15.0"
  },
  "devDependencies": {},
  "description": ""
}

```

### 1.2.4. NPM init und NPM install zum Konfigurieren und installieren wichtiger Server-Komponente

In der Konsole (Applications ==> System Tools ==> Terminator) im Ordner des UI5WebServer wurde die Package.json initialisiert und die wichtigen Module 'express' und 'mysql' installiert:

```bash
npm init && npm install --save-dev 
``` 

Die Node-Module des Servers sind nun im Ordner als neuer Ordner zu finden sein.

### 1.2.5. Der Server ist eine JavaScript-Datei, die folgt aussieht:

```javascript
/* Test server */

var express = require('express');
var mysql = require('mysql');

// init backend app
var app = express();

// database definition
var connection = mysql.createConnection({
  host: 'localhost',
  user: 's72898',
  password: 's72898',
  database: 'apl_ui5_testdb'
});

// connect to mysql testdb
connection.connect(function(err) {
  if(err) throw err;
  console.log('Connected...');
});

app.post('/getentries', function(request, response) {
  var mySqlQuery = request.query.mysqlquery; 
  var resultSet = [];

  connection.query(mySqlQuery, function(err, rows, fields) {
    if(err) {
      console.log(err);
      resultSet = err;
    } else {
      for(var i in rows) {
        console.log(rows[i]);
        resultSet.push(rows[i]);
      }
    }

    response.writeHead(200, "OK", {'Content-Type': 'text/plain'});
    response.write(JSON.stringify(resultSet));
    response.end();
  });
});

app.use(express.static('../UI5WebApp'));

app.listen(3000, function() {
  console.log('Webserver listening on port 3000...');
});
```

Die aktuelle Struktur sieht so aus: (Abb. [Struktur2](#struktur2))

[struktur2]: struktur2.png "Struktur2"
![Struktur2][struktur2]

## 1.3. Anpassung wichtiger Resourcen

Um den Ordner 'resources' einzubinden, muss der Namespace des Projekts in einzelnen Dateien sowie die Pfadangabe für 'resources' in der ```index.html``` angepasst werden. ```sap.ui.``` wird demnach hinzugefügt.

**Warum legt man den Namensraum nicht sofort bei der Projekterstellung an?**

Dies ist möglich, jedoch beim Entwickeln auf der SAPWebIDE nicht zwingend notwendig. Eine Definition des Namespaces, wie ```sap.ui.``` ist nicht möglich, da der letzte Punkt eine Fehlermeldung hervorruft. (Abb. [Namespace Error](#namespaceerror)) Beispielsweise ergibt sich aus Namespace und Projektname folgende Konstellation: ```sap.ui.UI5App```, dies ist zulässig und der Name der App ```ui5app``` ergibt ```sap.ui.UI5Appui5app```, was jedoch nicht zum gewünschten Ergebnis führt, den mit der richtigen Anpassung können nun UI5-System-Komponenten, also unsere 'resources', nutzen. Konvention werden hierbei ebenso eingehalten. 

[namespaceerror]: namespaceerror.png "Namespace Error"
![Namespace Error][namespaceerror]

### 1.3.1. Manifest.json

Im folgenden wird der Namespace in der Manifest für folgende Properties angepasst sowie nicht unterstützte Libraries entfernt:

```JSON
"sap.app": {
	"id": "sap.ui.UI5WebApp.webapp", // ANPASSUNG des Namespaces der App mit "sap.ui."
    ...
```

```JSON
"sap.ui5": {
	"rootView": {
		"viewName": "sap.ui.UI5WebApp.webapp.view.Home", // ANPASSUNG des Namespaces der App mit "sap.ui."
		"type": "XML"
	},
    "dependencies": {
	    "minUI5Version": "1.30.0",
	    "libs": {
            "sap.ui.core": {},
            "sap.m": {},
            "sap.ui.layout": {}
            // entfernen der Pakete:  "sap.collaboration": {}, "sap.ui.comp": {}, "sap.uxap": {}
            // da diese nicht eingebunden werden
	    }
    },
    ...
```

```JSON
"models": {
	"i18n": {
        "type": "sap.ui.model.resource.ResourceModel",
        "settings": {
            "bundleName": "sap.ui.UI5WebApp.i18n.i18n" // ANPASSUNG des Namespaces der Ressourcen für das Model mit "sap.ui." 
        }
    }
}
```

### 1.3.2. Index.html und Resourcen-Ordner einbinden

Nun werden Namespace und Pfad für 'resources' angepasst. 

```html
...
<!-- ANPASSEN der Namensräume -->
<!-- ** Anpassung des Ordners "resources" **-->
<script id="sap-ui-bootstrap"
    src="resources/sap-ui-core.js"
	data-sap-ui-libs="sap.m"
	data-sap-ui-theme="sap_bluecrystal"
	data-sap-ui-preload="async"
	data-sap-ui-resourceroots='{"sap.ui.UI5WebApp": "./"}'>
<script>
...
<script>
    sap.ui.getCore().attachInit(function() {
	    new sap.m.Shell({
		    app: new sap.ui.core.ComponentContainer({
			    height : "100%",
			    name : "sap.ui.UI5WebApp"
		    })
	    }).placeAt("content");
    });
</script>
...
```

### 1.3.3. Component.js

Anpassung in ```Component.js```:

```javascript
sap.ui.define([
	"sap/ui/core/UIComponent",
	"sap/ui/Device",
	"sap/ui/UI5WebApp/webapp/model/models" // Anpassung!
]
```

### 1.3.4. Home.controller.js und Home.view.xml

Anpassung in ```Home.controller.js``` und ```Home.view.xml```:

```javascript
sap.ui.define([
	"sap/ui/core/mvc/Controller"
], function(Controller) {
	"use strict";

	return Controller.extend("sap.ui.UI5WebApp.webapp.controller.Home", { // Anpassung!

	});
});
```

```xml
<!-- Anpassung! -->
<mvc:View controllerName="sap.ui.UI5WebApp.webapp.controller.Home" xmlns:html="http://www.w3.org/1999/xhtml" xmlns:mvc="sap.ui.core.mvc"
	displayBlock="true" xmlns="sap.m">
	<App>
		<pages>
			<Page title="{i18n>title}">
				<content></content>
			</Page>
		</pages>
	</App>
</mvc:View>
```

## 1.4. Starten der App

In der Konsole unter mit ```cd ~/ui5user/<PFAD DER APP>/UI5App/UI5WebServer/``` kann nun mit ```nodemon server.js``` die App gestartet werden: (Abb. [App nach Anpassung starten](#appnachanpassung))

[appnachanpassung]: appnachanpassung.png "App nach Anpassung starten"
![App nach Anpassung starten][appnachanpassung]

# 2.0. Entwicklung der UI5-Application

**Für die Entwicklung soll ein App-Template dienen!** 

Die App ist zu finden unter: ```~/ui5user/Template/UI5_App/```. Dort sind zwei Ordner hinterlegt: ```UI5_HANA_APL_server``` und ```UI5_HANA_APL```. Der Ordner ``` ~/ui5user/Template/UI5_App/UI5_HANA_APL ``` soll als Ausgangsbasis für die Entwicklung dienen und ist ähnlich mit jener aus Kapitel 1 aufgebaut.

In dieser Anpassung ist *Quellcode* schon vorgegeben, um die Arbeit zu erleichtern. Schließlich soll der Fokus auf das Entwickeln gelegt werden und sowie auf das DataBinding unter UI5. 

**Als aller erstes soll der Server mit ```nodemon ~/ui5user/Template/UI5_App/UI5_HANA_APL_server/server.js``` gestartet werden!**

## 2.1. Implementieren der ListUI-Komponente

Ziel dieser Episode soll sein: Wir implementieren die Landing Page, den ListUI.controller. (Abb. [ListUI](#listui))

[listui]: listui.png "ListUI"
![ListUI][listui]

Unsere View-Basis ist, wie bereits erwähnt, XML. Zu Beginn erstellen wir zwei Dateien. Im Ordner 
```
~/ui5user/Template/UI5_App/UI5_HANA_APL/ui5_hana_apl/controller
``` 

legen wir das Skript ```ListUI.controller.js```, im Ordner 

```
~/ui5user/Template/UI5_App/UI5_HANA_APL/ui5_hana_apl/view
``` 

die Datei ```ListUI.view.xml``` an. 

Zu Beginn implementieren wir unsere Grundstruktur in den ListUI-Controller:<br>
```javascript
sap.ui.define([
    'sap/ui/core/mvc/Controller',
    'sap/ui/UI5_HANA_APL/util/control/DataProvider',
    'sap/ui/model/json/JSONModel'
    //constructor:
], function(Controller, DataProvider, JSONModel) {
    var _oControllerId = 'sap.ui.UI5_HANA_APL.ui5_hana_apl.controller.ListUI';
    var self;

    // actually logic of hana db in stored procedures
    var LIST_QUERY = 'SELECT bm_name FROM zze_72898bm';

    function _fnOnPressOpenDataSetPage(oEvent) {
        // TODO
    }

    function _fnSetListItems() {
        // TODO
    }

    var OController = {
        onInit: function () {
            self = this;
            self.oView = this.getView();
            self.oList = self.oView.byId('__list0');
            self.dataProvider = new DataProvider();
        },
        onAfterRendering: function() {
            _fnSetListItems.call(self);
        }
    };

    return Controller.extend(_oControllerId, OController);
});
```

Der Controller kann, wie hier dargestellt, als Singleton-Pattern implementiert. Aus JavaScript kennt man dies bei der Erstellung klassicher Objekte:
```javascript
var Singleton = {
    myProp: 'propsValue',
    // ... some properties
}
```
Das hat den Sinn, dass für die MVC-Komponente je eine Instanz erstellt wird, mit denen wir arbeiten wollen. Mit ```self``` als ```this```-Referenz wollen wir gleich mehrfach weiterarbeiten. Im Constructor ist die Klasse ```DataProvider``` enthalten. Diese ist bereits implementiert und dient als Service für die Requests zur MySQL-Testdatenbank.

Jetzt implementieren wir erst einmal die ```ListUI.view```:
```xml
<mvc:XMLView
        viewName="sap.ui.UI5_HANA_APL.ui5_hana_apl.ListUI"
        controllerName="sap.ui.UI5_HANA_APL.ui5_hana_apl.controller.ListUI"
        xmlns="sap.m"
        xmlns:mvc="sap.ui.core.mvc">
    <IconTabBar selectedKey="__filter0" id="__bar0" expanded="true">
        <items>
            <IconTabFilter text="{i18n>ListTitle}" icon="sap-icon://list" iconColor="Default">
                <content>
                <!-- Data Binding at this list element-->
                    <List
                            noDataText="Drop list items here"
                            id="__list0"
                            class="sapUiResponsiveMargin"
                            width="auto">
                    </List>
                </content>
            </IconTabFilter>
            <IconTabFilter text="{i18n>CreateNewTree}" icon="sap-icon://create" iconColor="Default">
                <content>
                    <sap.ui.layout.form:Form editable="true" xmlns:sap.ui.layout.form="sap.ui.layout.form" id="__form2">
                        <sap.ui.layout.form:formContainers>
                            <sap.ui.layout.form:FormContainer title="CREATE NEW TREE" id="__containerCreateTree">
                                <sap.ui.layout.form:formElements>
                                    <sap.ui.layout.form:FormElement label="Trees Name" id="__element-create-tree">
                                        <sap.ui.layout.form:fields>
                                            <Input class="item-data" width="100%" id="__input-tree" value="" editable="true"/>
                                        </sap.ui.layout.form:fields>
                                        <sap.ui.layout.form:fields>
                                            <Button text="OK" type="Accept" width="100px" id="__create-tree-accept-button" press="onPressCreateNewAction" visible="true" />
                                        </sap.ui.layout.form:fields>
                                    </sap.ui.layout.form:FormElement>
                                </sap.ui.layout.form:formElements>
                            </sap.ui.layout.form:FormContainer>
                        </sap.ui.layout.form:formContainers>
                        <sap.ui.layout.form:layout>
                            <sap.ui.layout.form:ResponsiveGridLayout id="__layout3"/>
                        </sap.ui.layout.form:layout>
                        <sap.ui.layout.form:customData>
                            <sap.ui.core:CustomData key="w5g.dt.editable" value="true" xmlns:sap.ui.core="sap.ui.core" id="__data25"/>
                        </sap.ui.layout.form:customData>
                    </sap.ui.layout.form:Form>
                </content>
            </IconTabFilter>
        </items>
    </IconTabBar>
</mvc:XMLView>
```

Im IconTabFilter-Element ist eine Liste enthalten, diese wollen wir nun mit unseren Bäumen füllen. Dabei bedienen wir uns den Werkzeugen von UI5.

Zurück im Controller wollen wir nun die Private Function ```_fnSetListItems``` implementieren. Diese soll nach dem Rendern ausgeführt werden. In der Funktion ```onAfterRendering```, eine UI5-System-Function der Controllerkomponente, <i>invoken</i> wir die Funktion.

Die private Methode sieht nun wie folgt aus:
```javascript
function _fnSetListItems() {
    var that = this;

    // ---> 2. fnCallback as first class function variable
    var fnCallback = function(data) {
        var json = JSON.parse(data);
        var dataSetEntryNames = [];
        var oModel = new JSONModel();

        json.map(function(value) {
            dataSetEntryNames.push({ 'entry': value['bm_name'] });
        });

        oModel.setData({ 'entries': dataSetEntryNames });

        that.oList.bindItems({
            path: '/entries',
            template: new sap.m.StandardListItem({
                title: '{entry}',
                type : 'Navigation',
                // current instance to get router object above
                press: _fnOnPressOpenDataSetPage.bind(self)
            })
        });

        that.oView.setModel(oModel);
    };

    // ---> 1. invoke fnPost from dataProvider with sql query
    this.dataProvider.fnPost(fnCallback, LIST_QUERY);
}
```

(Bitte die TODOs im Quelltext durch die jeweiligen Kodeschnipsel ersetzen.)

Die Funktion ```this.dataProvider.fnPost``` wird invoked und hat als Argument die Variable ```fnCallback```, welches selbst eine Funktion ist und die als Parameter "data" die Ergebnisemenge der Anfrage an den DataProvider zurückbekommt. Diese Ergebnisemenge wollen wir nun in die Liste binden. Die Referenz der Liste wurde bereits im Controller aus der XML geholt:

```javascript
self.oView = this.getView();
self.oList = self.oView.byId('__list0');
```

Wir erstellen dafür ein oModel aus der System-Klasse JSONModel, definieren ein Array "dataSetEntryNames" und fügen ein Objekt mit der Property 'entry' ein. Dies brauchen wir im Anschluss.

```javascript
var json = JSON.parse(data);
var dataSetEntryNames = [];
var oModel = new JSONModel();

json.map(function(value) {
    dataSetEntryNames.push({ 'entry': value['bm_name'] });
});
```

Nun binden wir das Array an ein neues Objekt mit der Property ```entries``` an das oModel:
```javascript
oModel.setData({ 'entries': dataSetEntryNames });
```

Jetzt binden wir unser Objekt an unser Listen-Element aus der view.xml:
```javascript
that.oList.bindItems({
    // { 'entries': dataSetEntryNames }
    path: '/entries',
    template: new sap.m.StandardListItem({
        // loop for every entry from the array 'dataSetEntryNames'
        title: '{entry}',
        type : 'Navigation',
        // current instance to get router object above
        press: _fnOnPressOpenDataSetPage.bind(self)
    })
});

```

Markenzeichen von UI5 ist folgende Schreibweise für das Anzeigen gebundener Einträge: ```{<yourBindingContext>}```.

Die Information des gewählten Listeneintrags soll nun an eine Seite weitergegeben werden. Hier macht auch erst das Binding Sinn. Die ```value```-Variable soll gebunden werden, sodass diese auf der ```ItemDetailsTier1```-View verwendet werden kann. Dazu muss folgende private Methode nun implementiert werden:
```javascript
function _fnOnPressOpenDataSetPage(oEvent) {
    var value = oEvent.getSource().getTitle();
    var oRouter = sap.ui.core.UIComponent.getRouterFor(this);

    oRouter.navTo("itemdetailstier1", {
        itemdetails1: value
    }, false);
}
```
Nun müssen wir die Seite ```ItemDetailsTier1``` und den dazugehörigen Controller erstellen und den Route-Eintrag in der ```manifest.json``` definieren.

In der Datei ```Component.js``` muss folgender Eintrag unbedingt vorhanden sein:
```javascript
init : function () {
    //... some other code

    this.getRouter().initialize();
}
```

In der manifest.json (Abb. [Manifest1](#manifestitemtier1definition)) definieren wir nun folgendes:

[manifestitemtier1definition]: manifestitemtier1definition.png "Manifest1"
![Manifest1][manifestitemtier1definition]

Die Home.view.xml (Abb. [Home.view.xml](#homeview)) muss wie folgt aussehen:

[homeview]: homeview.png "Home.view.xml"
![Home.view.xml][homeview]

Ein Refresh erstellt folgende View. (Abb. [Home.view.xml](#homeview))

[listui]: listui.png "ListUI"
![ListUI][listui]

## 2.2. Implementieren der der ItemDetailsTier1-Ansicht

Nun wollen wir das Data-Binding forsetzen und die ItemDetailsTier1-Komponenten implementieren.

Das ```ItemDetailsTier1.controller.js``` soll zu Beginn folgendes enthalten:
```javascript
sap.ui.define([
    'sap/ui/core/mvc/Controller',
    'sap/ui/UI5_HANA_APL/util/control/DataProvider',
    //'sap/ui/UI5_HANA_APL/util/control/UIHandler'     // TODO activate me
// TODO implement UIHandler as third param
], function(Controller, DataProvider) { 
    var _oControllerId = 'sap.ui.UI5_HANA_APL.ui5_hana_apl.controller.ItemDetailsTier1';
    var self;
    
    // actually logic of hana db in stored procedures
    var SQL_JOIN_ON_LOAD = "SELECT * FROM zze_72898kt as kt JOIN zze_72898bm as bm ON" +
        " kt.bm_id = bm.bm_id WHERE bm.bm_name = ";

    function _fnGetQueryWithActionValue() {
        var query = SQL_JOIN_ON_LOAD + "'" + self.currentEntry + "'";
        var actionValue = self.oView.byId('__input-action').getValue();

        if(actionValue !== '') {
            query += " AND kt.action = '" + actionValue + "';";
        } else {
            query += ";";
        }

        return query;
    }

    var OController = {
        onInit: function () {
            self = this;
            self.oView = this.getView();
            
            // get dataProvider instance
            self.dataProvider = new DataProvider();

            // use instance of TableUI
            //self.uiHandler = new UIHandler();     // TODO activate me

            // get binded values from ListUI
            self.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
            self.oRouter
                .getRoute("itemdetailstier1")
                .attachPatternMatched(this.bindValue, this);
        },
        bindValue: function(oEvent) {
            // get selected entry from List / binded entry !!!
            self.currentEntry = oEvent.getParameter("arguments").itemdetails1;
            var id = this.getView().byId('__bm-name-title');
            // define sql join from both mysql tables 
            var query = SQL_JOIN_ON_LOAD + "'" + self.currentEntry + "';";

            id.setTitle(self.currentEntry);

            // some javascript magic  
            //var fnCallback = this.uiHandler.processData;     // TODO activate me

            // get all details from mysql you need
            //self.dataProvider.fnPost(fnCallback, query);     // TODO activate me
        },
        onPressAccept: function(oEvent) {
            var onInputChangeQueries = [];
            var id = self.oView.byId('__input-action');
            var query = _fnGetQueryWithActionValue();

            //self.dataProvider.fnPost(self.uiHandler.processData, query);     // TODO activate me
        },
        onPressReject: function(oEvent) {
            self.oRouter.navTo("home");
        }
    };

    return Controller.extend(_oControllerId, OController);
});
```

```self.oRouter``` wird definiert und ```attachPatternMatched``` ist eine System-Funktion, die eine Callback-Funktion übergeben bekommt. In diesem Fall ist die Callback-Funktion ```bindValue```, diese wird nach Initialisierung der Klasse ```ItemDetailsTier1.controller.js``` aus dem Pattern des übergeordneten Routes "abgefeuert". In ```bindValue``` ist der entscheidende Ausdruck,
```
self.currentEntry = oEvent.getParameter("arguments").itemdetails1; // => "BaumX"
```
der Moment, wo der gebundene Wert über das Event geholt wird. Dazu wollen wir gleich die ```ItemDetailsTier1.view.xml``` erstellen:
```xml
<mvc:XMLView
        viewName="sap.ui.UI5_HANA_APL.ui5_hana_apl.ItemDetailsTier1"
        controllerName="sap.ui.UI5_HANA_APL.ui5_hana_apl.controller.ItemDetailsTier1"
        xmlns="sap.m"
        xmlns:mvc="sap.ui.core.mvc">
    <Page
            title="{i18n>ItemDetails1}"
            showNavButton="true"
            navButtonPress="onPressReject">
        <content>
            <sap.ui.layout.form:Form editable="true" xmlns:sap.ui.layout.form="sap.ui.layout.form" id="__form4">
                <sap.ui.layout.form:formContainers>
                    <sap.ui.layout.form:FormContainer title="Title" id="__bm-name-title">
                        <!-- set tree name dynamically  -->
                    </sap.ui.layout.form:FormContainer>
                </sap.ui.layout.form:formContainers>
                <sap.ui.layout.form:layout>
                    <sap.ui.layout.form:ResponsiveGridLayout id="__layout0"/>
                </sap.ui.layout.form:layout>
            </sap.ui.layout.form:Form>
            <IconTabBar selectedKey="__filter3" id="__bar0" expanded="false">
                <items>
                    <IconTabFilter text="Filter" icon="sap-icon://add-filter" iconColor="Default">
                        <sap.ui.layout:Grid xmlns:sap.ui.layout="sap.ui.layout" id="__grid0">
                            <sap.ui.layout:content>
                                <sap.ui.layout.form:Form editable="true" xmlns:sap.ui.layout.form="sap.ui.layout.form" id="__form1">
                                    <sap.ui.layout.form:formContainers>
                                        <sap.ui.layout.form:FormContainer title="Aktion" id="__container6">
                                            <sap.ui.layout.form:formElements>
                                                <sap.ui.layout.form:FormElement id="__element6">
                                                    <sap.ui.layout.form:fields>
                                                        <Input width="100%" id="__input-action" change="onInputChange" />
                                                    </sap.ui.layout.form:fields>
                                                </sap.ui.layout.form:FormElement>
                                            </sap.ui.layout.form:formElements>
                                        </sap.ui.layout.form:FormContainer>
                                    </sap.ui.layout.form:formContainers>
                                    <sap.ui.layout.form:layout>
                                        <sap.ui.layout.form:ResponsiveGridLayout id="__layout2"/>
                                    </sap.ui.layout.form:layout>
                                    <sap.ui.layout.form:customData>
                                        <sap.ui.core:CustomData key="w5g.dt.editable" value="true" xmlns:sap.ui.core="sap.ui.core" id="__data23"/>
                                    </sap.ui.layout.form:customData></sap.ui.layout.form:Form>
                                </sap.ui.layout:content>
                        </sap.ui.layout:Grid>
                        <Bar>
                            <contentLeft>
                                <Button text="Cancel" type="Reject" width="100px" id="__tier1reject-button" press="onPressReject" />
                                <Button text="OK" type="Accept" width="100px" id="__tier1accept-button" press="onPressAccept"/>
                            </contentLeft>
                        </Bar>
                    </IconTabFilter>
                </items>
            </IconTabBar>
            <!--<mvc:XMLView viewName="sap.ui.UI5_HANA_APL.ui5_hana_apl.view.TableUI"/>-->
        </content>
    </Page>
</mvc:XMLView>
```

Nun sollte schon nach dem Selektieren eines Baumes in der Liste zur ItemDetailsTier1-Seite navigiert werden. (Abb. [ItemDetailsTier1OhneDetails](#itemdetailstier11))

[itemdetailstier11]: itemdetailstier11.png "ItemDetailsTier1OhneDetails"
![ItemDetailsTier1OhneDetails][itemdetailstier11]

## 2.3. Implementieren einer sap.ui.table.Table mit Data-Binding

Ziel dieser Episode ist, ein weiteres DataBinding zu erzeugen, sodass die Werte einer Zeile einer Tabelle übergeben werden. Die Seite Item Details 1 soll folgendes aussehen bekommen, welches unter Abbildung ItemDetailsTier1Full betrachtet werden kann. (Abb. [ItemDetailsTier1Full](#itemdetailstier1full))

[itemdetailstier1full]: itemdetailstier1full.png "ItemDetailsTier1Full"
![ItemDetailsTier1Full][itemdetailstier1full]

Im vorherigen Teil, ItemDetailsTier1.controller.js, soll nun Code einkommentiert werden, der die Hinweise TODO enthält:
```javascript
/* e.g. */
// TODO activate
```

Nun wollen wir die Dateien ```TableUI.controller.js``` und ```TableUI.view.xml``` erstellen. Die TableUI soll als eigenständiges Modul innerhalb der ItemDetailsTier1 fungieren, sodass wir zuerst in der ```ItemDetailsTier1.view.xml``` hinter ```</IconTabBar>``` folgendes einfügen: 
```xml
<mvc:XMLView viewName="sap.ui.UI5_HANA_APL.ui5_hana_apl.view.TableUI"/>
```

Nun erstellen wir die beiden eben genannten Files und fügen folgenden Code jeweils ein:
```xml
<mvc:View
        controllerName="sap.ui.UI5_HANA_APL.ui5_hana_apl.controller.TableUI"
        xmlns="sap.m"
        xmlns:mvc="sap.ui.core.mvc"
        xmlns:l="sap.ui.layout">

    <IconTabBar selectedKey="__filter2" id="__bar0">
        <items>
            <IconTabFilter text="Datenblatt" iconColor="Default" id="__filter2">
                <Panel id="action_table_panel"></Panel>
            </IconTabFilter></items>
    </IconTabBar>
</mvc:View>
```
und

```javascript
sap.ui.define([
    'sap/ui/core/mvc/Controller',
    'sap/ui/model/json/JSONModel',
    'sap/ui/UI5_HANA_APL/util/control/TableFactory'
], function(Controller, JSONModel, TableFactory) {
    var _oControllerId = 'sap.ui.UI5_HANA_APL.ui5_hana_apl.controller.TableUI';
    var self;
    var TABLE_ID = 'action_table_panel';

    var OController = {
        onInit: function() {
            self = this;
            self.tableFactory = new TableFactory();
            self.tableFactory.createDefaultOTable.call(self, TABLE_ID);
        },
        updateTable: function(json) {
            self.tableFactory.updateOTable.call(self, json);
        },
        constructor: function() {}
    };

    return Controller.extend(_oControllerId, OController);
});
```

Das Control ```TableFactory.js``` stellt der Signaturen ```createDefaultOTable``` und ```updateOTable``` zur Verfügung. Auf dieses Control wollen wir uns nun konzentrieren, da sich hier die größte Musik abspielt:
```javascript
sap.ui.define([
    'sap/ui/core/Control',
    'sap/ui/model/json/JSONModel',
    'sap/m/MessageToast'
], function(Control, JSONModel, MessageToast) {
    var ICON_TABFILTER_ID = '__filter2';

    // default values to fill the table
    var DEFAULT_COLUMN_NAME = [
        { columnName: "bm_id" },
        { columnName: "action_date" },
        { columnName: "action" }
    ];
    var DEFAULT_ROW_DATA = [{
        bm_id : "Test ID",
        action_date : "01022002",
        action : "Test action"
    }];

    // process response 
    function _fnFillOTableDynamically(json) {
        var keys = json[0];
        var fieldnames = [];
        var rows = [];

        Object.keys(keys).map(function(fieldname) {
            fieldnames.push({ 'columnName': fieldname });
        });

        json.forEach(function(value, key) {
            var rowObject = {};
            Object.keys(json[key]).map(function(elem) {
                rowObject[elem] = json[key][elem];
            });
            rows.push(rowObject);
        });

        return [ fieldnames, rows ];
    }

    /*function _getRowDetail(oEvent) {
        var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
        var rowIndex = oEvent.getParameters("arguments")['rowIndex'];

        oRouter.navTo("itemdetailstier2", { itemdetails2: rowIndex });
    }*/

    var OControl = {
        createDefaultOTable: function(id) {
            // TODO
        },
        updateOTable: function(json, id) {
            // TODO
        }
    };

    return Control.extend('sap.ui.UI5_HANA_APL.util.control.TableFactory', OControl);
});
```

Nach und nach wollen wir nun die beiden Klassenmethoden implementieren. In ```createDefaultOTable``` starten wir mit dem Erstellen eines ```JSONModels``` und der Erstellung einer Instanz der Klasse ```sap.ui.table.Table```:
```javascript
var oModel = new JSONModel();
var that = this;

// is known from ListUI.controller.js
oModel.setData({
    rows: DEFAULT_ROW_DATA,
    columns: DEFAULT_COLUMN_NAME
});

this.oTable = new sap.ui.table.Table({
    id: id,
    selectionMode: sap.ui.table.SelectionMode.Single, // select single row
    cellClick: function(oEvent) {
        // fired in click
        //_getRowDetail.call(that, oEvent)
    }
});
// bind oModel into oTable
this.oTable.setModel(oModel);
```
Das Model ist erstellt, die Daten darin gebunden. Der Tabelle werden mit ```oTable.setModel``` das Model zur Verfügung gestellt. Nun wollen wir auch definieren, wie die Daten in der Tabelle eingefügt werden:

```javascript
this.oTable.bindColumns('/columns', function (sId, oContext) {
    // 'oContext.getObject().columnName' from setted oModel: 
    // DEFAULT_COLUMN_NAME = [ { columnName: "bm_id" }, ...
    var columnName = oContext.getObject().columnName;
    // bind column names into Column(s)
    return new sap.ui.table.Column({
        label: columnName,
        template: columnName
    });
});
// bind rows / values
this.oTable.bindRows('/rows');

// add table into XML-View
this.getView().byId(ICON_TABFILTER_ID).addContent(this.oTable);
```

Die Methode ```updateOTable``` soll, wie der Name schon sagt, die bestehende Table-Instanz aktualisieren:
```javascript
// json as response from mysql db
// sql syntax was an error:
if(json['sqlMessage']) {
    MessageToast.show(json['sqlMessage']);
} else {
    try {
        var tableProperties = _fnFillOTableDynamically(json);

        // get model from oTable instance
        var oModel = this.oTable.getModel();

        // bind updated data
        oModel.setData({
            rows: tableProperties[1],
            columns: tableProperties[0]
        });

        // update oTable's model definition
        this.oTable.setModel(oModel);

        // set global, necessary for ItemDetailsTier2
        sap.ui.getCore().setModel(oModel, 'tabledata');
    } catch(err) {
        MessageToast.show('Please check your input!');
    }
}
```

In den Controls unter ```~/ui5user/Template/UI5_App/UI5_HANA_APL/util/control``` ist der UIHandler zu finden und hat nun wie folgt auszusehen:
```javascript
sap.ui.define([
    'sap/ui/core/Control',
    //'sap/ui/UI5_HANA_APL/ui5_hana_apl/controller/TableUI.controller'
// TODO set second param 'TableUI'
], function(Control, TableUI) { 
    var self; // instance table from current sales order

    var OControl = {
        constructor: function () {
            self = this;
            self.fnUpdateTableCallback = new TableUI().updateTable;
        },
        processData: function(data) {
            var json = JSON.parse(data);
            self.fnUpdateTableCallback(json);
        }
    };

    return Control.extend('sap.ui.UI5_HANA_APL.util.control.UIHandler', OControl);
});

```

Nun sollte sich die Seite aktualisieren lassen und die Tabelle samt Inhalt aus dem SQL-Join anzeigen.

## 2.4. Implementieren der ItemDetailsTier2

Zu Beginn soll die private Methodedeklaration ```_getRowDetail```  in ```TableFactory.js```, sowie das Usage
```
this.oTable = new sap.ui.table.Table({
    id: id,
    selectionMode: sap.ui.table.SelectionMode.Single, // select single row
    cellClick: function(oEvent) {
        // fired in click
        //_getRowDetail.call(that, oEvent)
    }
});
```
**entkommentiert** werden. 

<i>Danach definieren wir ```itemdetailstier2``` in der ```manifest.json```, ähnlich wie für ItemDetailsTier1, was bereits geschah.</i> 

Nun erstellen wir die beiden View- und Controller-Komponente:
```javascript
sap.ui.define([
    'sap/ui/core/mvc/Controller',
    'sap/ui/core/routing/History',
    'sap/ui/model/json/JSONModel',
    'sap/ui/UI5_HANA_APL/util/control/NavToPrevious',
    'sap/ui/UI5_HANA_APL/util/control/DataProvider',
    'sap/m/MessageToast'
], function(Controller, History, JSONModel, NavToPrevious, DataProvider, MessageToast) {
    var _oControllerId = 'sap.ui.UI5_HANA_APL.ui5_hana_apl.controller.ItemDetailsTier2';
    var self;
    var ROW_MODEL = 'row';

    // ...in hana: stored procedure
    var CREATE_ACTION_QUERY = 'INSERT INTO zze_72898kt (bm_id, action) VALUES ((SELECT bm_id FROM zze_72898bm WHERE bm_id = ';

    // TODO should be a generic function inside of index.html
    function _fnParseValues(json) {
        var parsedObject = {};

        Object.keys(json).map(function(item) {
            var parsedValue = typeof value !== 'string'
                ? json[item].toString()
                : json[item];

            parsedObject[item] = parsedValue;
        });

        return parsedObject;
    }

    function _fnOnInitEditableFalse() {
        this.oView.byId('__input2').setEditable(false);
        this.oView.byId('__input3').setEditable(false);
        this.oView.byId('__update-cancel-button').setVisible(false);
        this.oView.byId('__update-accept-button').setVisible(false);
    }

    var OController = {
        onInit: function () {
            self = this;
            self.oView = this.getView();
            self.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
            self.prevPage = new NavToPrevious();
            self.dataProvider = new DataProvider();

            self.oRouter
                .getRoute("itemdetailstier2")
                .attachPatternMatched(this.bindSelectedRowData, this);
        },
        bindSelectedRowData: function (oEvent) {
            // TODO

            _fnOnInitEditableFalse.call(this);
        },
        onPressSetEditableForUpdate: function(oEvent) {
            //self.oView.byId('__input1').setEditable(true);
            //self.oView.byId('__input4').setEditable(true);
            self.oView.byId('__input2').setEditable(true);
            self.oView.byId('__input3').setEditable(true);
            self.oView.byId('__update-cancel-button').setVisible(true);
            self.oView.byId('__update-accept-button').setVisible(true);
        },
        onPressCreateNewAction: function(oEvent) {
            var oModel = self.oView.getModel(ROW_MODEL);
            var treeId = oModel.getData()['bm_id'];
            var action = self.oView.byId('__input-action').getValue();
            var query = CREATE_ACTION_QUERY + treeId + '), \'' + action + '\');';

            self.dataProvider.fnPost(function(success) {
                var isCreated = JSON.parse(success);

                if(isCreated[6]) {
                    MessageToast.show('Action wurde erstellt!');
                } else {
                    MessageToast.show('Fehler!');
                }
            }, query);
        },
        onPressRejectToTier1: function(oEvent) {
            _fnOnInitEditableFalse.call(self);
        },
        onPressBack: function(oEvent) {
            self.prevPage.goToPrevPage(self.oRouter, 'itemdetailstier1');
        }
    };

    return Controller.extend(_oControllerId, OController);
});
```

Die Methode ```bindSelectedRowData``` wollen wir gleich implementieren, vorher definieren wir unsere View-File:

```xml
<mvc:XMLView
        viewName="sap.ui.UI5_HANA_APL.ui5_hana_apl.ItemDetailsTier2"
        controllerName="sap.ui.UI5_HANA_APL.ui5_hana_apl.controller.ItemDetailsTier2"
        xmlns="sap.m"
        xmlns:mvc="sap.ui.core.mvc">
    <Page
            title="{i18n>ItemDetails2}"
            showNavButton="true"
            navButtonPress="onPressBack">
        <content>
            <sap.ui.layout.form:Form editable="true" xmlns:sap.ui.layout.form="sap.ui.layout.form" id="__form4">
                <sap.ui.layout.form:formContainers>
                    <sap.ui.layout.form:FormContainer title="{row>/bm_name}" id="__container1">
                        <sap.ui.layout.form:formElements>
                            <sap.ui.layout.form:FormElement label="BmID" id="__element1">
                                <sap.ui.layout.form:fields>
                                    <Input class="item-data" width="100%" id="__input1" value="{row>/bm_id}" editable="false"/>
                                </sap.ui.layout.form:fields>
                            </sap.ui.layout.form:FormElement>
                        </sap.ui.layout.form:formElements>
                        <sap.ui.layout.form:formElements>
                            <sap.ui.layout.form:FormElement label="Action" id="__element2">
                                <sap.ui.layout.form:fields>
                                    <Input class="item-data" width="100%" id="__input2" value="{row>/action}" editable="false"/>
                                </sap.ui.layout.form:fields>
                            </sap.ui.layout.form:FormElement>
                        </sap.ui.layout.form:formElements>
                        <sap.ui.layout.form:formElements>
                            <sap.ui.layout.form:FormElement label="ActionDate" id="__element3">
                                <sap.ui.layout.form:fields>
                                    <Input class="item-data" width="100%" id="__input3" value="{row>/action_date}" editable="false"/>
                                </sap.ui.layout.form:fields>
                            </sap.ui.layout.form:FormElement>
                        </sap.ui.layout.form:formElements>
                        <sap.ui.layout.form:formElements>
                            <sap.ui.layout.form:FormElement label="CreationDate" id="__element4">
                                <sap.ui.layout.form:fields>
                                    <Input class="item-data" width="100%" id="__input4" value="{row>/creation_date}" editable="false"/>
                                </sap.ui.layout.form:fields>
                            </sap.ui.layout.form:FormElement>
                        </sap.ui.layout.form:formElements>
                    </sap.ui.layout.form:FormContainer>
                </sap.ui.layout.form:formContainers>
                <sap.ui.layout.form:layout>
                    <sap.ui.layout.form:ResponsiveGridLayout id="__layout0"/>
                </sap.ui.layout.form:layout>
            </sap.ui.layout.form:Form>
            <Bar>
                <contentRight>
                    <Button text="Cancel" type="Reject" width="100px" id="__update-cancel-button" press="onPressRejectToTier1" visible="false" />
                    <Button text="OK" type="Accept" width="100px" id="__update-accept-button" press="onPressAccept" visible="false" />
                </contentRight>
            </Bar>
            <IconTabBar selectedKey="__filter3" id="__bar0" expanded="false">
                <items>
                    <IconTabFilter text="Edit" icon="sap-icon://edit" iconColor="Default">
                        <sap.ui.layout:Grid xmlns:sap.ui.layout="sap.ui.layout" id="__grid0">
                            <sap.ui.layout:content>
                                <sap.ui.layout.form:Form editable="true" xmlns:sap.ui.layout.form="sap.ui.layout.form" id="__form1">
                                    <sap.ui.layout.form:formContainers>
                                        <sap.ui.layout.form:FormContainer title="UPDATE" id="__container6">
                                            <sap.ui.layout.form:formElements>
                                                <sap.ui.layout.form:FormElement id="__element6">
                                                    <sap.ui.layout.form:fields>
                                                        <Button text="UPDATE" type="Default" width="100px" id="__editable-true-button" press="onPressSetEditableForUpdate" />
                                                    </sap.ui.layout.form:fields>
                                                </sap.ui.layout.form:FormElement>
                                            </sap.ui.layout.form:formElements>
                                        </sap.ui.layout.form:FormContainer>
                                    </sap.ui.layout.form:formContainers>
                                    <sap.ui.layout.form:layout>
                                        <sap.ui.layout.form:ResponsiveGridLayout id="__layout2"/>
                                    </sap.ui.layout.form:layout>
                                    <sap.ui.layout.form:customData>
                                        <sap.ui.core:CustomData key="w5g.dt.editable" value="true" xmlns:sap.ui.core="sap.ui.core" id="__data23"/>
                                    </sap.ui.layout.form:customData>
                                </sap.ui.layout.form:Form>
                                <sap.ui.layout.form:Form editable="true" xmlns:sap.ui.layout.form="sap.ui.layout.form" id="__form2">
                                    <sap.ui.layout.form:formContainers>
                                        <sap.ui.layout.form:FormContainer title="CREATE NEW ACTION" id="__container7">
                                            <sap.ui.layout.form:formElements>
                                                <sap.ui.layout.form:FormElement label="Action" id="__element-action">
                                                    <sap.ui.layout.form:fields>
                                                        <Input class="item-data" width="100%" id="__input-action" value="" editable="true"/>
                                                    </sap.ui.layout.form:fields>
                                                    <sap.ui.layout.form:fields>
                                                        <Button text="OK" type="Accept" width="100px" id="__create-action-accept-button" press="onPressCreateNewAction" visible="true" />
                                                    </sap.ui.layout.form:fields>
                                                </sap.ui.layout.form:FormElement>
                                            </sap.ui.layout.form:formElements>
                                        </sap.ui.layout.form:FormContainer>
                                    </sap.ui.layout.form:formContainers>
                                    <sap.ui.layout.form:layout>
                                        <sap.ui.layout.form:ResponsiveGridLayout id="__layout3"/>
                                    </sap.ui.layout.form:layout>
                                    <sap.ui.layout.form:customData>
                                        <sap.ui.core:CustomData key="w5g.dt.editable" value="true" xmlns:sap.ui.core="sap.ui.core" id="__data24"/>
                                    </sap.ui.layout.form:customData>
                                </sap.ui.layout.form:Form>
                            </sap.ui.layout:content>
                        </sap.ui.layout:Grid>
                    </IconTabFilter>
                </items>
            </IconTabBar>
        </content>
    </Page>
</mvc:XMLView>
```
Um das Binding auch zu nutzen, dass heißt, die Daten anzeigen zu lassen, wird wiederholt folgende Konvention in den XML-Tags angewandt (bitte nicht eintragen):
```xml
<!-- BaumID --->
<Input class="item-data" width="100%" id="__input1" value="{row>/bm_id}" editable="false"/>

<!--Action--->
<Input class="item-data" width="100%" id="__input2" value="{row>/action}" editable="false"/>
```

```bindSelectedRowData``` wird enthält nun folgendes:
```javascript
// defined model from itemdetailstier2
var oModel = sap.ui.getCore().getModel('tabledata');
// which row is selected?
var rowIndex = oEvent.getParameter("arguments").itemdetails2;
// get data from row
var row = oModel.oData.rows[rowIndex];
// process data
var parsedRow = fnParseValues(row); 
var jsonModel = new JSONModel(parsedRow);

self.getView().setModel(jsonModel, ROW_MODEL);

// initialize elements as editable = false 
_fnOnInitEditableFalse.call(this);
```

Nun kann das Ergebnise betrachtet werden. (Abb. [ItemDetailsTier2Full](#itemdetailstier2full))

[itemdetailstier2full]: itemdetailstier2full.png "ItemDetailsTier2Full"
![ItemDetailsTier2Full][itemdetailstier2full]
