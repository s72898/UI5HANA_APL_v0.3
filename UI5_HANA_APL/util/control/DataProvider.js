sap.ui.define([
    "sap/ui/core/Control"
], function(Control) {
    "use strict";

    var URL = '/getentries?mysqlquery=';

    // public
    var OControl = {
        constructor: function() {},
        fnPost: function(fnCallback, data) {
            $.post(URL + data, null, fnCallback);
        }
    };

    return Control.extend('sap.ui.UI5_HANA_APL.ui5_hana_apl.util.control.Ajax', OControl);
});
