sap.ui.define([
    'sap/ui/core/Control',
    'sap/ui/UI5_HANA_APL/ui5_hana_apl/controller/TableUI.controller'
], function(Control, TableUI) {
    var self; // instance table from current sales order

    var OControl = {
        constructor: function () {
            self = this;

            // start create TableUI at new TableUI()
            self.fnUpdateTableCallback = new TableUI().updateTable;
        },
        processData: function(data) {
            var json = JSON.parse(data);
            self.fnUpdateTableCallback(json);
        }
    };

    return Control.extend('sap.ui.UI5_HANA_APL.util.control.UIHandler', OControl);
});
