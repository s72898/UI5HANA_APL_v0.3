sap.ui.define([
    'sap/ui/core/Control',
    'sap/ui/model/json/JSONModel',
    'sap/m/MessageToast'
], function(Control, JSONModel, MessageToast) {
    var ICON_TABFILTER_ID = '__filter2';
    var DEFAULT_COLUMN_NAME = [
        { columnName: "bm_id" },
        { columnName: "action_date" },
        { columnName: "action" }
    ];
    var DEFAULT_ROW_DATA = [{
        bm_id : "Test ID",
        action_date : "01022002",
        action : "Test action"
    }];

    function _fnFillOTableDynamically(json) {
        var keys = json[0];
        var fieldnames = [];
        var rows = [];

        Object.keys(keys).map(function(fieldname) {
            fieldnames.push({ 'columnName': fieldname });
        });

        json.forEach(function(value, key) {
            var rowObject = {};
            Object.keys(json[key]).map(function(elem) {
                rowObject[elem] = json[key][elem];
            });
            rows.push(rowObject);
        });

        return [ fieldnames, rows ];
    }

    function _getRowDetail(oEvent) {
        var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
        var rowIndex = oEvent.getParameters("arguments")['rowIndex'];

        // rowIndex as id of global setted model
        oRouter.navTo("itemdetailstier2", { itemdetails2: rowIndex });
    }

    var OControl = {
        createDefaultOTable: function(id) {
            var oModel = new JSONModel();
            var that = this;

            oModel.setData({
                rows: DEFAULT_ROW_DATA,
                columns: DEFAULT_COLUMN_NAME
            });

            this.oTable = new sap.ui.table.Table({
                id: id,
                selectionMode: sap.ui.table.SelectionMode.Single,
                cellClick: function(oEvent) {
                    _getRowDetail.call(that, oEvent)
                }
            });
            this.oTable.setModel(oModel);

            this.oTable.bindColumns('/columns', function (sId, oContext) {
                var columnName = oContext.getObject().columnName;
                return new sap.ui.table.Column({
                    label: columnName,
                    template: columnName
                });
            });
            this.oTable.bindRows('/rows');

            this.getView().byId(ICON_TABFILTER_ID).addContent(this.oTable);
        },
        updateOTable: function(json) {
            if(json['sqlMessage']) {
                MessageToast.show(json['sqlMessage']);
            } else {
                try {
                    var tableProperties = _fnFillOTableDynamically(json);
                    var oModel = this.oTable.getModel();

                    oModel.setData({
                        rows: tableProperties[1],
                        columns: tableProperties[0]
                    });

                    this.oTable.setModel(oModel);

                    /**** set global ****/
                    sap.ui.getCore().setModel(oModel, 'tabledata');
                } catch(err) {
                    MessageToast.show('Please check your input!');
                }
            }
        }
    };

    return Control.extend('sap.ui.UI5_HANA_APL.util.control.TableFactory', OControl);
});
