sap.ui.define([
    'sap/ui/core/mvc/Controller',
    'sap/ui/model/json/JSONModel',
    'sap/ui/UI5_HANA_APL/util/control/TableFactory'
], function(Controller, JSONModel, TableFactory) {
    var _oControllerId = 'sap.ui.UI5_HANA_APL.ui5_hana_apl.controller.TableUI';
    var self;
    var TABLE_ID = 'action_table_panel';

    var OController = {
        // load before onAfterRendering; load after "new"
        onInit: function() {
            self = this;

            self.tableFactory = new TableFactory();
            self.tableFactory.createDefaultOTable.call(self, TABLE_ID);
        },
        updateTable: function(json) {
            self.tableFactory.updateOTable.call(self, json);
        }
    }

    return Controller.extend(_oControllerId, OController);
});
