sap.ui.define([
    'sap/ui/core/mvc/Controller',
    'sap/ui/UI5_HANA_APL/util/control/DataProvider',
    'sap/ui/UI5_HANA_APL/util/control/UIHandler'
], function(Controller, DataProvider, UIHandler) {
    var _oControllerId = 'sap.ui.UI5_HANA_APL.ui5_hana_apl.controller.ItemDetailsTier1';
    var self;
    var SQL_JOIN_ON_LOAD = "SELECT * FROM zze_72898kt as kt JOIN zze_72898bm as bm ON" +
        " kt.bm_id = bm.bm_id WHERE bm.bm_name = "; // TODO --> Backend Code

    function _fnGetQueryWithActionValue() {
        var query = SQL_JOIN_ON_LOAD + "'" + this.currentEntry + "'";
        var actionValue = this.oView.byId('__input-action').getValue();

        if(actionValue !== '') {
            query += " AND kt.action = '" + actionValue + "';";
        } else {
            query += ";";
        }

        return query;
    }

    var OController = {
        onInit: function () {
            self = this;
            self.oView = this.getView();
            self.dataProvider = new DataProvider();
            self.uiHandler = new UIHandler();
            self.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
            self.oRouter
                .getRoute("itemdetailstier1")
                .attachPatternMatched(this.fnBindValue, this);
        },
        fnBindValue: function(oEvent) {
            self.currentEntry = oEvent.getParameter("arguments").itemdetails1;
            var id = this.getView().byId('__bm-name-title');
            var query = SQL_JOIN_ON_LOAD + "'" + self.currentEntry + "';";

            id.setTitle(self.currentEntry);

            var fnCallback = this.uiHandler.processData;
            self.dataProvider.fnPost(fnCallback, query);
        },
        // Tier1 OK Button
        onPressAccept: function(oEvent) {
            var onInputChangeQueries = [];
            var id = self.oView.byId('__input-action');
            var query = _fnGetQueryWithActionValue.call(self);

            self.dataProvider.fnPost(self.uiHandler.processData, query);
        },
        onPressReject: function(oEvent) {
            self.oRouter.navTo("home");
        }
    };

    return Controller.extend(_oControllerId, OController);
});
