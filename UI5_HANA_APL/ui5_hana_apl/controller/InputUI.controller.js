sap.ui.define([
    'sap/ui/core/mvc/Controller',
    'sap/ui/UI5_HANA_APL/util/control/DataProvider',
    'sap/ui/UI5_HANA_APL/util/control/UIHandler'
], function(Controller, DataProvider, UIHandler) {
    var _oControllerId = 'sap.ui.UI5_HANA_APL.ui5_hana_apl.controller.InputUI';
    var self;

    var OController = {
        onInit: function () {
            self = this;

            var oView = this.getView();
            self.crudTextArea = oView.byId('crud_text_area');

            self.dataProvider = new DataProvider();
            self.uiHandler = new UIHandler();
        },
        getQueryOnPress: function() {
            var query = self.crudTextArea.getValue();
            var fnCallback = this.uiHandler.processData;
            self.dataProvider.fnPost(fnCallback, query);
        }/*,
        onSelectionChange: function(oEvent) {
            var id = oEvent.getSource().getSelectedItem().getId();
            var chartId = id.split('--')[1];
            var sqlTable = chartId.split('-')[0];
            var selectAllQuery = 'SELECT * FROM ' + sqlTable;

            _fnExecuteQuery.call(self, selectAllQuery);
        }*/
    };

    return Controller.extend(_oControllerId, OController);
});