sap.ui.define([
    'sap/ui/core/mvc/Controller',
    'sap/ui/UI5_HANA_APL/util/control/DataProvider',
    'sap/ui/model/json/JSONModel'
], function(Controller, DataProvider, JSONModel) {
    var _oControllerId = 'sap.ui.UI5_HANA_APL.ui5_hana_apl.controller.ListUI';
    var self;
    var LIST_QUERY = 'SELECT bm_name FROM zze_72898bm'; // TODO --> Backend Code

    function _fnOnPressOpenDataSetPage(oEvent) {
        var value = oEvent.getSource().getTitle();

        // this as instance to go back to the last page
        var oRouter = sap.ui.core.UIComponent.getRouterFor(this);

        oRouter.navTo("itemdetailstier1", {
            itemdetails1: value
        }, false);
    }

    function _fnSetListItems() {
        var that = this;

        var fnCallback = function(data) {
            var json = JSON.parse(data);
            var dataSetEntryNames = [];
            var oModel = new JSONModel();

            json.map(function(value) {
                dataSetEntryNames.push({ 'entry': value['bm_name'] });
            });

            oModel.setData({ 'entries': dataSetEntryNames });

            that.oList.bindItems({
                path: '/entries',
                template: new sap.m.StandardListItem({
                    title: '{entry}',
                    type : 'Navigation',
                    // current instance to get router object above
                    press: _fnOnPressOpenDataSetPage.bind(self)
                })
            });

            that.oView.setModel(oModel);
        };

        this.dataProvider.fnPost(fnCallback, LIST_QUERY);
    }

    var OController = {
        onInit: function () {
            self = this;
            self.oView = this.getView();
            self.oList = self.oView.byId('__list0');
            self.dataProvider = new DataProvider();
        },
        onAfterRendering: function() {
            _fnSetListItems.call(self);
        }
    };

    return Controller.extend(_oControllerId, OController);
});
