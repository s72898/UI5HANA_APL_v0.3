sap.ui.define([
    'sap/ui/core/mvc/Controller',
    'sap/ui/core/routing/History',
    'sap/ui/model/json/JSONModel',
    'sap/ui/UI5_HANA_APL/util/control/NavToPrevious',
    'sap/ui/UI5_HANA_APL/util/control/DataProvider',
    'sap/m/MessageToast'
], function(Controller, History, JSONModel, NavToPrevious, DataProvider, MessageToast) {
    var _oControllerId = 'sap.ui.UI5_HANA_APL.ui5_hana_apl.controller.ItemDetailsTier2';
    var self;
    var ROW_MODEL = 'row'; // private constant of binding model in XML
    var CREATE_ACTION_QUERY = 'INSERT INTO zze_72898kt (bm_id, action) VALUES ((SELECT bm_id FROM zze_72898bm WHERE bm_id = ';

    function _fnOnInitEditableFalse() {
        this.oView.byId('__input2').setEditable(false);
        this.oView.byId('__input3').setEditable(false);
        this.oView.byId('__update-cancel-button').setVisible(false);
        this.oView.byId('__update-accept-button').setVisible(false);
    }

    function _fnParseValues(json) {
        var parsedObject = {};

        Object.keys(json).map(function(item) {
            var parsedValue = typeof value !== 'string'
                ? json[item].toString()
                : json[item];

            parsedObject[item] = parsedValue;
        });

        return parsedObject;
    }

    var OController = {
        onInit: function () {
            self = this;
            self.oView = this.getView();
            self.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
            self.prevPage = new NavToPrevious();
            self.dataProvider = new DataProvider();

            self.oRouter
                .getRoute("itemdetailstier2")
                .attachPatternMatched(this.bindSelectedRowData, this);
        },
        bindSelectedRowData: function (oEvent) {
            var oModel = sap.ui.getCore().getModel('tabledata');
            var rowIndex = oEvent.getParameter("arguments").itemdetails2;
            var row = oModel.oData.rows[rowIndex];
            var parsedRow = _fnParseValues(row);
            var jsonModel = new JSONModel(parsedRow);

            self.getView().setModel(jsonModel, ROW_MODEL);

            _fnOnInitEditableFalse.call(this);
        },
        onPressSetEditableForUpdate: function(oEvent) {
            //self.oView.byId('__input1').setEditable(true);
            //self.oView.byId('__input4').setEditable(true);
            self.oView.byId('__input2').setEditable(true);
            self.oView.byId('__input3').setEditable(true);
            self.oView.byId('__update-cancel-button').setVisible(true);
            self.oView.byId('__update-accept-button').setVisible(true);
        },
        onPressCreateNewAction: function(oEvent) {
            var oModel = self.oView.getModel(ROW_MODEL);
            var treeId = oModel.getData()['bm_id'];
            var action = self.oView.byId('__input-action').getValue();
            var query = CREATE_ACTION_QUERY + treeId + '), \'' + action + '\');';

            self.dataProvider.fnPost(function(success) {
                var isCreated = JSON.parse(success);

                if(isCreated[6]) {
                    MessageToast.show('Action wurde erstellt!');
                } else {
                    MessageToast.show('Fehler!');
                }
            }, query);
        },
        onPressRejectToTier1: function(oEvent) {
            _fnOnInitEditableFalse.call(self);
        },
        onPressBack: function(oEvent) {
            self.prevPage.goToPrevPage(self.oRouter, 'itemdetailstier1');
        }
    };

    return Controller.extend(_oControllerId, OController);
});
